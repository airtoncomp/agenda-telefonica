/**
 * 2017, Agenda Telefonica
 * Airton Ishimori (nobumasa.bcc@gmail.com)
 * */

package com.tarefa.agendatelefone.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.tarefa.agendatelefone.model.Contato;
import com.tarefa.agendatelefone.model.Contatos;

//@Controller
@RestController
public class ContatosController {
	
	@Autowired
	private Contatos contatos;
	
	@GetMapping("/")
	public ModelAndView index () {
		return listar();
	}
	
	@GetMapping("/contatos")
	public ModelAndView listar () {
		ModelAndView mv = new ModelAndView("AgendaTelefonica");
		mv.addObject("contatos", contatos.findAll());
		mv.addObject(new Contato());
		return mv;
	}

	
	@RequestMapping(value = "/execucao", params = "remover", method = RequestMethod.GET)
	public ModelAndView remover (Contato contato) {
		contatos.delete(contatos.findOne(contato.getId()));
		return listar();
	}
	@RequestMapping(value = "/execucao", params = "atualizar", method = RequestMethod.GET)
	public ModelAndView editar (Contato contato) {
		ModelAndView mv = new ModelAndView("EdicaoContato");
		mv.addObject("contatos", contatos.findOne((contato.getId())));
		return mv;
	}
	
	
	@PostMapping("/cadastro")
	public RedirectView salvar (Contato contato) {
		if (!contato.getNome().isEmpty() && !contato.getTelefone().isEmpty()) {
			Boolean repetido = false;
			Iterator<Contato> it = contatos.findAll().iterator();
			while (it.hasNext()) {
				Contato c = it.next();
				if (contato.getTelefone().trim().contentEquals(c.getTelefone().trim())
						|| contato.getNome().trim().contentEquals(c.getNome().trim())) {
					repetido = true;
					break;
				}
			} 
			if (!repetido && contato.getTelefone().trim().matches("[0-9]+")) 
				contatos.save(contato);
		}
		return new RedirectView("/contatos", true);
	}
	
	@PostMapping("/atualizacao")
	public RedirectView atualizar (Contato contato) {
		if (contatos.findOne(contato.getId()) != null) 
			contatos.delete(contato.getId());
		if (!contato.getNome().isEmpty() && !contato.getTelefone().isEmpty()) {
			Boolean repetido = false;
			Iterator<Contato> it = contatos.findAll().iterator();
			while (it.hasNext()) {
				Contato c = it.next();
				if (contato.getTelefone().trim().contentEquals(c.getTelefone().trim())
						|| contato.getNome().trim().contentEquals(c.getNome().trim())) {
					repetido = true;
					break;
				}
			} 
			if (!repetido && contato.getTelefone().trim().matches("[0-9]+")) 
				contatos.save(contato);
		}
		return new RedirectView("/contatos", true);
	}
	
	@GetMapping("/pesquisa")
	public ModelAndView pesquisar (Contato contato) {
		ModelAndView mv = new ModelAndView("AgendaTelefonica");
		List<Contato> contatosPesquisa = new ArrayList<Contato>();
		for (Iterator<Contato> it = contatos.findAll().iterator(); it.hasNext(); ) {
			Contato c = it.next();
			if (c.getNome().trim().contains(contato.getNome().trim())) {
				contatosPesquisa.add(c);
			}
		}
		mv.addObject("contatos", contatosPesquisa);
		return mv;
	}
}
