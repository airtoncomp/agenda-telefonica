/**
 * 2017, Agenda Telefonica
 * Airton Ishimori (nobumasa.bcc@gmail.com)
 * */

package com.tarefa.agendatelefone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgendaTelefonicaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgendaTelefonicaApplication.class, args);
	}
}
