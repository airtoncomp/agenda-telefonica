/**
 * 2017, Agenda Telefonica
 * Airton Ishimori (nobumasa.bcc@gmail.com)
 * */

package com.tarefa.agendatelefone.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Contato implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private Long id;
	private String nome;
	private String telefone;
	
	//Setters
	public void setId (Long id) { this.id = id; }
	public void setNome (String nome) { this.nome = nome; }
	public void setTelefone (String telefone) { this.telefone = telefone; }
	
	//Getters
	public Long getId () { return id; }
	public String getNome () { return nome; }
	public String getTelefone () { return telefone; }
}
