/**
 * 2017, Agenda Telefonica
 * Airton Ishimori (nobumasa.bcc@gmail.com)
 * */

package com.tarefa.agendatelefone.model;

import org.springframework.data.repository.CrudRepository;

public interface Contatos extends CrudRepository<Contato, Long> {

}


/*
import org.springframework.data.jpa.repository.JpaRepository;

public interface Contatos extends JpaRepository<Contato, Long> {

}
*/

