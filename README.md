TAREFA: Agenda Telefônica
==========================

Realiza operações de cadastro, recuperação, atualização e remoção de contatos da agenda telefônica

- Uma vez que o projeto estiver em execução, o mesmo pode ser acessado no endereço https://localhost:8080.

- Desenvolvido com o ambiente de desenvolvimento STS (Spring Tool Suite)
- Necessita que o PostgreSQL esteja instalado e em execução no sistema operacional
- Testes foram realizados na plataforma Solus OS (distro Linux) e no navegador Chrome. 
